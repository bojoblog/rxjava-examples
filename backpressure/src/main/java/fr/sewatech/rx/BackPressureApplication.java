package fr.sewatech.rx;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;

import java.util.stream.Stream;

import static fr.sewatech.rx.Helper.message;
import static fr.sewatech.rx.Helper.sleep;

public class BackPressureApplication {

    private static final BackPressureApplication singleton = new BackPressureApplication();
    private static final AppLifecycle lifecycle = new AppLifecycle();
    private static final int MAX_COUNT = 1_000;

    public static void main(String[] args) {
        lifecycle.letsGo();
        RunMode.from(args).main.run();
        lifecycle.waitUntilTheEnd();
    }

    private void mainSyncObservable() {
        Observable
                .range(0, MAX_COUNT)
                .map(Data::new)
                .subscribe(
                        this::handleNext,
                        this::handleError,
                        lifecycle::thisIsTheEnd
                );
    }

    private void mainAsyncObservable() {
        Observable
                .range(0, 1_000)
                .map(Data::new)
                .observeOn(Schedulers.computation(), false, 32)
                .subscribe(
                        this::handleNext,
                        this::handleError,
                        lifecycle::thisIsTheEnd
                );
    }

    private void mainFlowable() {
        Flowable
                .range(0, 1_000)
                .map(Data::new)
                .observeOn(Schedulers.computation())
                .subscribe(
                        this::handleNext,
                        this::handleError,
                        lifecycle::thisIsTheEnd
                );
    }

    private void handleNext(Data data) {
        sleep(data.value == 0 ? 500 : 50);
        System.out.println(message(data));
    }

    private void handleError(Throwable throwable) {
        System.err.println(message(throwable));
    }

    private enum RunMode {
        OBSERVABLE_SYNC(BackPressureApplication.singleton::mainSyncObservable),
        OBSERVABLE_ASYNC(BackPressureApplication.singleton::mainAsyncObservable),
        FLOWABLE_ASYNC(BackPressureApplication.singleton::mainFlowable);

        private final Runnable main;

        RunMode(Runnable main) {
            this.main = main;
        }

        static RunMode from(String[] args) {
            return Stream.of(args)
                    .findFirst()
                    .map(text -> text.replace('-', '_'))
                    .map(String::toUpperCase)
                    .map(RunMode::valueOf)
                    .orElse(OBSERVABLE_SYNC);
        }
    }

}
